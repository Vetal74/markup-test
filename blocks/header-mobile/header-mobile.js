// Пиши скрипты сюда
//Aga

/*
 * @props:
 * @sideMenuClass - Класс бокового меню
 * @sideMenuActiveClass - Класс "Активного" бокового меню
 * @sideMenuFlyClass - Класс дополнительных элементов, которые вылетают справа
 */
function SideMenu(props) {
    if(this.isUndefined(props)) props = {};
    this.sideProps = {
        sideMenuClass: this.setPrevent(props.sideMenuClass, 'side-menu--main'),
        sideMenuActiveClass: this.setPrevent(props.sideMenuActiveClass, 'active'),
        sideMenuFlyClass: this.setPrevent(props.sideMenuFlyClass, 'active')
    };
}
SideMenu.prototype = {
    open: function(callback) {
        $('.' + this.sideProps.sideMenuClass).addClass(this.sideProps.sideMenuActiveClass);
        $('body').css('overflow', 'hidden');
        if(typeof callback === 'function') callback();
    },
    close: function(callback) {
        $('.' + this.sideProps.sideMenuClass).removeClass(this.sideProps.sideMenuActiveClass);
        $('body').css('overflow', 'auto');
        //Если доп. меню открыто, то закрываем его в другую сторону
        var $flyMenuActive = $('.side-fly.active');
        if($flyMenuActive.length > 0) {
            $flyMenuActive.addClass('close');
            setTimeout(function() {
                $flyMenuActive.removeClass('active close');
            }, 350);
        }
        if(typeof callback === 'function') callback();
    },
    //Функционал открытия доп. секций
    flyMenuOpen: function(flyMenu) {
        try {
            if(this.isUndefined(flyMenu)) throw 'Не заполнен дата-атрибут';
            var $flyMenu = $('.' + flyMenu);
            if($flyMenu.length < 1) throw 'Дата атрибут заполнен не верно, такого меню нет';

            $flyMenu.addClass(this.sideProps.sideMenuFlyClass);
        } catch (err) {
            //console.error(new Error(err));
            console.error(err);
        }
    },
    flyMenuClose: function(closeArrow) {
        $(closeArrow).parents('.side-fly').removeClass('active');
    },
    setPrevent: function(get, prevent) {
        if(typeof get == 'undefined') return prevent;
        else return get;
    },
    isUndefined: function(x) {
        return typeof x == 'undefined';
    }
};
$(function() {
    var sideMenu = new SideMenu();
    var $body = $('body');
    $body.on('click', '.header__menu.header-icon', function() {
        var $this = $(this);
        if($this.hasClass('active')) {
            $this.removeClass('active');
            sideMenu.close();
        } else {
            $this.addClass('active');
            sideMenu.open();
        }
        return false;
    });
    $body.on('click', '.side-menu__link--arrow', function() {
        var flyMenu = $(this).data('fly');
        sideMenu.flyMenuOpen(flyMenu);
    });
    $body.on('click', '.side-menu__link--arrow-close', function() {
        sideMenu.flyMenuClose($(this));
    });
});